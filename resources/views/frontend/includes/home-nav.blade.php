<!-- NAV start -->
<header>
    <div id="sticker" class="header-area">
        <div class="container">
            <div class="row">
                <!-- logo start -->
                <div class="col-md-2 col-lg-4">
                    <div class="logo">
                        {{ link_to_route('frontend.index', app_name(), [], ['class' => 'navbar-brand']) }}
                    </div>
                </div>
                <!-- logo end -->
                <!-- search start -->
                {{--<div class="col-md-2 col-lg-2">--}}
                    {{--<form action="index-4.html#">--}}
                        {{--<span class="search-button"><i class="fa fa-search"></i></span>--}}
                        {{--<input type="text" placeholder="search..."/>--}}
                    {{--</form>--}}
                {{--</div>--}}
                <!-- search end -->
                <!-- mainmenu start -->
                <div class="col-md-10 col-lg-8">
                    <div class="mainmenu">
                        <nav>
                            <ul id="nav">
                                <li><a href="index.html">Home</a>
                                    <ul class="sub-menu">
                                        <li><a href="index.html">Homepage Version 2</a></li>
                                        <li><a href="index.html">Homepage Version 3</a></li>
                                        <li><a href="index.html">Homepage Version 4</a></li>
                                    </ul>
                                </li>
                                @if (config('locale.status') && count(config('locale.languages')) > 1)
                                    <li>
                                        <a href="#">
                                            {{ trans('menus.language-picker.language') }}
                                            <span class="caret"></span>
                                        </a>

                                        @include('includes.partials.lang')
                                    </li>
                                @endif
                                @if ($logged_in_user)
                                    <li>{{ link_to_route('frontend.user.dashboard', trans('navs.frontend.dashboard'), [], ['class' => active_class(Active::checkRoute('frontend.user.dashboard')) ]) }}</li>
                                @endif

                                @if (! $logged_in_user)
                                    <li>{{ link_to_route('frontend.auth.login', trans('navs.frontend.login'), [], ['class' => active_class(Active::checkRoute('frontend.auth.login')) ]) }}</li>

                                    @if (config('access.users.registration'))
                                        <li>{{ link_to_route('frontend.auth.register', trans('navs.frontend.register'), [], ['class' => active_class(Active::checkRoute('frontend.auth.register')) ]) }}</li>
                                    @endif
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            {{ $logged_in_user->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            @permission('view-backend')
                                            <li>{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration')) }}</li>
                                            @endauth
                                            <li>{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account'), [], ['class' => active_class(Active::checkRoute('frontend.user.account')) ]) }}</li>
                                            <li>{{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                                        </ul>
                                    </li>
                                @endif

                                <li>{{ link_to_route('frontend.contact', trans('navs.frontend.contact'), [], ['class' => active_class(Active::checkRoute('frontend.contact')) ]) }}</li>

                                {{--<li><a href="blog-column-3.html">Blog</a>--}}
                                    {{--<ul class="sub-menu">--}}
                                        {{--<li><a href="blog-column-3.html">Blog page</a></li>--}}

                                        {{--<li><a href="blog-details.html">Blog Single Item</a></li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- mainmenu end -->
            </div>
        </div>
    </div>
    <!-- mobile-menu-area start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mobile-menu">
                        <div class="logo">
                            <a href="index.html"><span style="font-size: 22px; color:#e74c3c; font-weight:bold;">U</span><span style="color:#f1c40f">DOTONG</span>
                            </a>
                        </div>
                        <nav id="dropdown">
                            <ul id="nav2">
                                <li><a href="index.html">Home</a>
                                    <ul class="sub-menu">
                                        <li><a href="index.html">Homepage Version 2</a></li>
                                        <li><a href="index.html">Homepage Version 3</a></li>
                                        <li><a href="index.html">Homepage Version 4</a></li>
                                    </ul>
                                </li>
                                <li><a href="portfolio-column-3.html">Portfolio</a>
                                </li>
                                <li><a href="our-office.html">Office</a></li>
                                <li><a href="blog-column-3.html">Blog</a>
                                    <ul class="sub-menu">
                                        <li><a href="blog-column-3.html">Blog page</a></li>

                                        <li><a href="blog-details.html">Blog Single Item</a></li>
                                    </ul>
                                </li>


                                <li><a href="about-us.html">About Me</a>
                                </li>

                                <li><a href="contact-us.html">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- mobile-menu-area end -->
</header>
<!-- header end -->