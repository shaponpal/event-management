@extends('frontend.layouts.home')

@section('content')
    <!-- HOME SLIDER -->
    @include('includes.partials.slider')
    <!-- END HOME SLIDER -->
    <!-- START EVENT AREA-->
    @include('includes.partials.event')
    <!-- END EVENT AREA-->

    <!-- creative-intensy-area start -->
    <div class="creative-intensy-area fix">
        <div class="container">
            <div class="row">
                <!-- creative-img start -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 hidden-sm">
                    <div class="creative-img wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                        @include('includes.partials.maps')
                    </div>
                </div>
                <!-- creative-img end -->
                <!-- creative-text start -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="creative-text">
                        <div class="creative-heading wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".5s">
                            <h1>CREATIVE <strong>Bootstrap</strong></h1>
                            <p>I am so happy, my dear friend, so absorbed in the exquisite sense of mere. few stray gleams steal.</p>
                        </div>
                        <div class="creative-info wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".7s">
                            <div class="creative-icon">
                                <i class="fa fa-chevron-right"></i>
                            </div>
                            <div class="creative-desc">
                                <h3>SUPER SMART SOLUTION</h3>
                                <p>A wonderful serenity has taken possession of my entire soul like these sweet  mornings spring which I enjoy with my whole heart I am alone.</p>
                            </div>
                        </div>
                        <div class="creative-info wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".9s">
                            <div class="creative-icon">
                                <i class="fa fa-chevron-right"></i>
                            </div>
                            <div class="creative-desc">
                                <h3>RUDICROUSLY EASY TO INSTALL</h3>
                                <p>A wonderful serenity has taken possession of my entire soul like these sweet  mornings spring which I enjoy with my whole heart I am alone.</p>
                            </div>
                        </div>
                        <div class="creative-info wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.1s">
                            <div class="creative-icon">
                                <i class="fa fa-chevron-right"></i>
                            </div>
                            <div class="creative-desc">
                                <h3>FULLY LOADED WITH SHORTCODES</h3>
                                <p>A wonderful serenity has taken possession of my entire soul like these sweet  mornings spring which I enjoy with my whole heart I am alone.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- creative-text end -->
            </div>
        </div>
    </div>
    <!-- creative-intensy-area end -->
    <!-- collapse-area start -->
    <div class="collapse-area fix">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="collapse-content wow fadeInDown" data-wow-duration="1.5s" data-wow-delay=".3s">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="index-2.html#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Be cool & stay true
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        Boy desirous families prepared gay reserved add ecstatic say. Replied joy age visitor nothing cottage
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="index-2.html#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Prospect at me wandered curiosity
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quiae
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="index-2.html#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Be careful & never lose a round
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingfour">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="index-2.html#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                            Always stay top of the game
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                    <div class="panel-body">
                                        Boy desirous families prepared gay reserved add ecstatic say. Replied joy age visitor nothing cottage
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingfive">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="index-2.html#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                            Effect if in up no depend impossible
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfive">
                                    <div class="panel-body">
                                        Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="macbook-content wow fadeInDown" data-wow-duration="1.5s" data-wow-delay=".6s">
                        <img src="img/features/macbook.png" alt="" />
                        <div class="macbook-list">
                            <div class="macbook-img">
                                <img src="img/features/m3.jpg" alt="" />
                            </div>
                            <div class="macbook-img">
                                <img src="img/features/m2.jpg" alt="" />
                            </div>
                            <div class="macbook-img">
                                <img src="img/features/m1.jpg" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- collapse-area end -->
    <!-- happy-clients-area start -->
    <div class="happy-clients-area fix">
        <div class="container">
            <div class="row clients-space">
                <div class="col-md-12">
                    <h1 class="heading-2"><strong>OUR HAPPY</strong>  CLIENTS</h1>
                    <div class="client-brand-list">
                        <div class="sigle-clients-brand">
                            <a href="index-2.html#"><img src="img/brand/1.svg" alt="" /></a>
                        </div>
                        <div class="sigle-clients-brand">
                            <a href="index-2.html#"><img src="img/brand/2.svg" alt="" /></a>
                        </div>
                        <div class="sigle-clients-brand">
                            <a href="index-2.html#"><img src="img/brand/3.svg" alt="" /></a>
                        </div>
                        <div class="sigle-clients-brand">
                            <a href="index-2.html#"><img src="img/brand/4.svg" alt="" /></a>
                        </div>
                        <div class="sigle-clients-brand">
                            <a href="index-2.html#"><img src="img/brand/5.svg" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="client-brand-list">
                        <div class="sigle-clients-brand">
                            <a href="index-2.html#"><img src="img/brand/6.svg" alt="" /></a>
                        </div>
                        <div class="sigle-clients-brand">
                            <a href="index-2.html#"><img src="img/brand/7.svg" alt="" /></a>
                        </div>
                        <div class="sigle-clients-brand">
                            <a href="index-2.html#"><img src="img/brand/8.svg" alt="" /></a>
                        </div>
                        <div class="sigle-clients-brand">
                            <a href="index-2.html#"><img src="img/brand/9.svg" alt="" /></a>
                        </div>
                        <div class="sigle-clients-brand">
                            <a href="index-2.html#"><img src="img/brand/10.svg" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- happy-clients-area end -->






    <!-- counter-area start -->
    <div class="counter-area fix">
        <div class="container">
            <div class="row">
                <!-- single-counter start -->
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="single-counter wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".3s">
                        <h1 class="counter">12798</h1>
                        <h3>TOTAL SELLS</h3>
                    </div>
                </div>
                <!-- single-counter end -->
                <!-- single-counter start -->
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="single-counter wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".5s">
                        <h1 class="counter">29</h1>
                        <h3>TOTAL BRANDS</h3>
                    </div>
                </div>
                <!-- single-counter end -->
                <!-- single-counter start -->
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="single-counter wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".7s">
                        <h1 class="counter">500</h1>
                        <h3>TOTAL PRODUCTS</h3>
                    </div>
                </div>
                <!-- single-counter end -->
                <!-- single-counter start -->
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="single-counter wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".9s">
                        <h1 class="counter">75</h1>
                        <h3>TOTAL MEMBERS</h3>
                    </div>
                </div>
                <!-- single-counter end -->
            </div>
        </div>
    </div>
    <!-- counter-area end -->
    <!-- features-area start -->
    <div class="features-area-1 fix">
        <div class="container">
            <div class="row">
                <!-- single-features start -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="single-features-1 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                        <div class="features-icon">
                            <i class="fa fa-css3"></i>
                        </div>
                        <div class="features-info-text">
                            <h3>AWESOME CSS3</h3>
                            <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which.</p>
                        </div>
                    </div>
                </div>
                <!-- single-features end -->
                <!-- single-features start -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="single-features-1 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".5s">
                        <div class="features-icon">
                            <i class="fa fa-html5"></i>
                        </div>
                        <div class="features-info-text">
                            <h3>VALID HTML5</h3>
                            <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which.</p>
                        </div>
                    </div>
                </div>
                <!-- single-features end -->
                <!-- single-features start -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="single-features-1 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".7s">
                        <div class="features-icon">
                            <i class="fa fa-joomla"></i>
                        </div>
                        <div class="features-info-text">
                            <h3>POWERED BY JOOMLA</h3>
                            <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which.</p>
                        </div>
                    </div>
                </div>
                <!-- single-features end -->
            </div>
        </div>
    </div>
    <!-- features-area end -->

    <!-- creative-member-area start -->
    <div class="creative-member-area fix">
        <div class="container">
            <!-- section-heading start -->
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                        <h1>OUR CREATIVE <strong>MEMBER</strong></h1>
                        <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy <br /> with my whole heart. I am alone, and feel the charm of existence in this spot, which was created</p>
                    </div>
                </div>
            </div>
            <!-- section-heading end -->
            <div class="row">
                <!-- single-creative-member start -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="single-creative-member wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                        <div class="member-photo">
                            <img src="img/team/m1.jpg" alt="" />
                        </div>
                        <div class="member-info">
                            <span class="member-name">Elvera Faulkner</span>
                            <span class="member-role">Developer</span>
                        </div>
                        <div class="member-icons">
                            <a href="index.html#"><i class="fa fa-facebook"></i></a>
                            <a href="index.html#"><i class="fa fa-twitter"></i></a>
                            <a href="index.html#"><i class="fa fa-google-plus"></i></a>
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="52px" height="52px" viewBox="0 0 52 52" enable-background="new 0 0 52 52" xml:space="preserve"><path d="M51.673,0H0v51.5c0.244-5.359,3.805-10.412,7.752-13.003l36.169-23.74c4.264-2.799,7.761-8.663,7.752-14.297V0L51.673,0z"/></svg>
                        </div>
                    </div>
                </div>
                <!-- single-creative-member end -->
                <!-- single-creative-member start -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="single-creative-member wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".5s">
                        <div class="member-photo">
                            <img src="img/team/m2.jpg" alt="" />
                        </div>
                        <div class="member-info">
                            <span class="member-name">Cherri Portnoy</span>
                            <span class="member-role">Programmer</span>
                        </div>
                        <div class="member-icons">
                            <a href="index.html#"><i class="fa fa-facebook"></i></a>
                            <a href="index.html#"><i class="fa fa-twitter"></i></a>
                            <a href="index.html#"><i class="fa fa-google-plus"></i></a>
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="52px" height="52px" viewBox="0 0 52 52" enable-background="new 0 0 52 52" xml:space="preserve"><path d="M51.673,0H0v51.5c0.244-5.359,3.805-10.412,7.752-13.003l36.169-23.74c4.264-2.799,7.761-8.663,7.752-14.297V0L51.673,0z"/></svg>
                        </div>
                    </div>
                </div>
                <!-- single-creative-member end -->
                <!-- single-creative-member start -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="single-creative-member wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".7s">
                        <div class="member-photo">
                            <img src="img/team/m3.jpg" alt="" />
                        </div>
                        <div class="member-info">
                            <span class="member-name">Jorge Mincey</span>
                            <span class="member-role">Designer</span>
                        </div>
                        <div class="member-icons">
                            <a href="index.html#"><i class="fa fa-facebook"></i></a>
                            <a href="index.html#"><i class="fa fa-twitter"></i></a>
                            <a href="index.html#"><i class="fa fa-google-plus"></i></a>
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="52px" height="52px" viewBox="0 0 52 52" enable-background="new 0 0 52 52" xml:space="preserve"><path d="M51.673,0H0v51.5c0.244-5.359,3.805-10.412,7.752-13.003l36.169-23.74c4.264-2.799,7.761-8.663,7.752-14.297V0L51.673,0z"/></svg>
                        </div>
                    </div>
                </div>
                <!-- single-creative-member end -->
            </div>
        </div>
    </div>
    <!-- creative-member-area end -->
    <!-- our-skill-area start -->
    <div class="our-skill-area fix">
        <div class="container">
            <!-- section-heading start -->
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h1>OUR AMAZING <strong>SKILL</strong></h1>
                    </div>
                </div>
            </div>
            <!-- section-heading end -->
            <div class="row">
                <!-- single-skill start -->
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="single-skill wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                        <div class="progress-circular">
                            <input type="text" class="knob" value="0" data-rel="85" data-linecap="round" data-width="175" data-bgcolor="#444444" data-fgcolor="#2BCDC1" data-thickness=".10" data-readonly="true" disabled>
                            <h4 class="progress-h4">Web Design</h4>
                        </div>
                    </div>
                </div>
                <!-- single-skill end -->
                <!-- single-skill start -->
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="single-skill wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".5s">
                        <div class="progress-circular">
                            <input type="text" class="knob" value="0" data-rel="75" data-linecap="round" data-width="175" data-bgcolor="#444444" data-fgcolor="#2BCDC1" data-thickness=".10" data-readonly="true" disabled>
                            <h4 class="progress-h4">Graphics Design</h4>
                        </div>
                    </div>
                </div>
                <!-- single-skill end -->
                <!-- single-skill start -->
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="single-skill wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".7s">
                        <div class="progress-circular">
                            <input type="text" class="knob" value="0" data-rel="65" data-linecap="round" data-width="175" data-bgcolor="#444444" data-fgcolor="#2BCDC1" data-thickness=".10" data-readonly="true" disabled>
                            <h4 class="progress-h4">PHP Developer</h4>
                        </div>
                    </div>
                </div>
                <!-- single-skill end -->
                <!-- single-skill start -->
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="single-skill wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".9s">
                        <div class="progress-circular">
                            <input type="text" class="knob" value="0" data-rel="55" data-linecap="round" data-width="175" data-bgcolor="#444444" data-fgcolor="#2BCDC1" data-thickness=".10" data-readonly="true" disabled>
                            <h4 class="progress-h4">Java Script</h4>
                        </div>
                    </div>
                </div>
                <!-- single-skill end -->
            </div>
        </div>
    </div>
    <!-- our-skill-area end -->
    <!-- video-area start -->
    <div class="video-area fix">
        <div class="container">
            <div class="row">
                <!-- video-player start -->
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="video-player wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".7s">
                        <video controls>
                            <source src="video/video.mp4" type="video/mp4">
                        </video>
                    </div>
                </div>
                <!-- video-player end -->
                <!-- skill start -->
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="skill-content-3">
                        <div class="section-heading-3">
                            <h2>OUR COMPANY STORY</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum quasi eum inventore ut, quod unde minus dolorum veniam soluta recusandae cum dignissimos illo. In facilis voluptate amet! Deserunt vitae repellendus ex, consequatur.</p>
                        </div>
                        <div class="skill">
                            <!-- PROGRESS START -->
                            <div class="progress">
                                <div class="lead">Web Design</div>
                                <div class="progress-bar wow fadeInLeft" data-progress="95%" style="width: 95%;" data-wow-duration="1.5s" data-wow-delay="1.2s"> <span>95%</span></div>
                            </div>
                            <!-- PROGRESS END -->
                            <!-- PROGRESS START -->
                            <div class="progress">
                                <div class="lead">Web Development</div>
                                <div class="progress-bar wow fadeInLeft" data-progress="85%" style="width: 85%;" data-wow-duration="1.5s" data-wow-delay="1.2s"><span>85%</span> </div>
                            </div>
                            <!-- PROGRESS END -->
                            <!-- PROGRESS START -->
                            <div class="progress">
                                <div class="lead">Graphics Design</div>
                                <div class="progress-bar wow fadeInLeft" data-progress="70%" style="width: 70%;" data-wow-duration="1.5s" data-wow-delay="1.2s"><span>70%</span> </div>
                            </div>
                            <!-- PROGRESS END -->
                        </div>
                    </div>
                    <!-- skill end -->
                </div>
            </div>
        </div>
    </div>
    <!-- video-area end -->
    <!-- testimonial-area start -->
    <div class="testimonial-area fix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="testimonial-curosel">
                        <div class="single-testimonial">
                            <div class="test-img">
                                <img src="img/testimonial/team1.jpg" alt="" />
                            </div>
                            <div class="test-text">
                                <p>The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about magazine and housed in a nice, gilded frame. It showed</p>
                            </div>
                            <div class="test-info">
                                <span class="test-name">Calista Rose</span>
                                <span class="test-title">General Maneger</span> -
                                <a href="index.html#">Shopnobuilder Ltd</a>
                            </div>
                        </div>
                        <div class="single-testimonial">
                            <div class="test-img">
                                <img src="img/testimonial/team2.jpg" alt="" />
                            </div>
                            <div class="test-text">
                                <p>It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table raising a heavy fur.</p>
                            </div>
                            <div class="test-info">
                                <span class="test-name">Jhone Doe</span>
                                <span class="test-title">Developer</span> -
                                <a href="index.html#">Shopnobuilder Ltd</a>
                            </div>
                        </div>
                        <div class="single-testimonial">
                            <div class="test-img">
                                <img src="img/testimonial/team3.jpg" alt="" />
                            </div>
                            <div class="test-text">
                                <p>"We would like to thank bdthemes ltd for an outstanding effort on this recently completed project located in the Moscow. The project involved a very aggressive schedule and it was completed on time. We would certainly like to use their professional services again."</p>
                            </div>
                            <div class="test-info">
                                <span class="test-name">Younis Khan</span>
                                <span class="test-title">Web Designer</span> -
                                <a href="index.html#">Shopnobuilder Ltd</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- testimonial-area end -->
    <!-- offer-area start -->
    <div class="offer-area fix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading-3 wow fadeInLeft" data-wow-duration=".8s" data-wow-delay=".1s">
                        <h2>WHAT WE OFFER</h2>
                        <p>Waters made blessed creature dry fish have creature wherein living also fill from air can't <br />to and it. That fruitful night very fourth firmament own. </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- single-offer start -->
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="single-offer wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".3s">
                        <div class="offer-icon">
                            <i class="fa fa-code"></i>
                        </div>
                        <div class="offer-text">
                            <h3>SHORTCODE ULTIMATE</h3>
                            <p>Separated they live in Bookmarks grove right at the coast of the Semantics</p>
                        </div>
                    </div>
                </div>
                <!-- single-offer end -->
                <!-- single-offer start -->
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="single-offer wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".5s">
                        <div class="offer-icon">
                            <i class="fa fa-eye"></i>
                        </div>
                        <div class="offer-text">
                            <h3>RETINA READY</h3>
                            <p>Separated they live in Bookmarks grove right at the coast of the Semantics</p>
                        </div>
                    </div>
                </div>
                <!-- single-offer end -->
                <!-- single-offer start -->
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="single-offer wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".7s">
                        <div class="offer-icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="offer-text">
                            <h3>USERS FRIENDLY</h3>
                            <p>Separated they live in Bookmarks grove right at the coast of the Semantics</p>
                        </div>
                    </div>
                </div>
                <!-- single-offer end -->
                <!-- single-offer start -->
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="single-offer wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".9s">
                        <div class="offer-icon">
                            <i class="fa fa-heart"></i>
                        </div>
                        <div class="offer-text">
                            <h3>CRAFTED WITH LOVE</h3>
                            <p>Separated they live in Bookmarks grove right at the coast of the Semantics</p>
                        </div>
                    </div>
                </div>
                <!-- single-offer end -->
                <!-- single-offer start -->
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="single-offer wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="1.1s">
                        <div class="offer-icon">
                            <i class="fa fa-tablet"></i>
                        </div>
                        <div class="offer-text">
                            <h3>100% RESPONSIVE LAYOUT</h3>
                            <p>Separated they live in Bookmarks grove right at the coast of the Semantics</p>
                        </div>
                    </div>
                </div>
                <!-- single-offer end -->
                <!-- single-offer start -->
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="single-offer wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="1.3s">
                        <div class="offer-icon">
                            <i class="fa fa-gears"></i>
                        </div>
                        <div class="offer-text">
                            <h3>FULLY CUSTOMIZIBLE</h3>
                            <p>Separated they live in Bookmarks grove right at the coast of the Semantics</p>
                        </div>
                    </div>
                </div>
                <!-- single-offer end -->
                <!-- single-offer start -->
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="single-offer wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="1.5s">
                        <div class="offer-icon">
                            <i class="fa fa-thumbs-o-up"></i>
                        </div>
                        <div class="offer-text">
                            <h3>MODERN BROWSER</h3>
                            <p>Separated they live in Bookmarks grove right at the coast of the Semantics</p>
                        </div>
                    </div>
                </div>
                <!-- single-offer end -->
                <!-- single-offer start -->
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="single-offer wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="1.7s">
                        <div class="offer-icon">
                            <i class="fa fa-key"></i>
                        </div>
                        <div class="offer-text">
                            <h3>UNLIMITED OPTIONS</h3>
                            <p>Separated they live in Bookmarks grove right at the coast of the Semantics</p>
                        </div>
                    </div>
                </div>
                <!-- single-offer end -->
                <!-- single-offer start -->
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="single-offer wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="1.9s">
                        <div class="offer-icon">
                            <i class="fa fa-exchange"></i>
                        </div>
                        <div class="offer-text">
                            <h3>MORE FLEXIBLE SLIDER</h3>
                            <p>Separated they live in Bookmarks grove right at the coast of the Semantics</p>
                        </div>
                    </div>
                </div>
                <!-- single-offer end -->
            </div>
        </div>
    </div>
    <!-- offer-area end -->


    <div class="latest-blog-area fix">
        <!-- top blog start -->
        <div class="even">
            <a class="blog-more" href="index.html#"><i class="fa fa-plus"></i></a>
            <a class="latest-blog-img" href="index.html#" style="background-image: url(img/blog/latest-blog/5.jpg);">
                <span class="image-over-style"></span>
            </a>
            <div class="latest-blog-info">
                <h3><a href="index.html#">I feel the presence of the Almighty</a></h3>
                <p>Far far away, behind the word…</p>
            </div>
        </div>
        <div class="even">
            <a class="blog-more" href="index.html#"><i class="fa fa-plus"></i></a>
            <a class="latest-blog-img" href="index.html#" style="background-image: url(img/blog/latest-blog/3.jpg);">
                <span class="image-over-style"></span>
            </a>
            <div class="latest-blog-info">
                <h3><a href="index.html#">Familiar with the countless</a></h3>
                <p>Far far away, behind the word…</p>
            </div>
        </div>
        <div class="latest-blog-desc">
            <div class="blog-text">
                <h3 class="latest-blog-title">Our Blog Post</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ornare <br /> tortor lorem, sit amet efficitur sapien imperdiet non. <br />  Donec posuere maximus facilisis. </p>
                <a class="readon border white uk-margin-top" href="index.html#">View All Works</a>
            </div>
        </div>
        <!-- top blog end -->
        <!-- bottom blog start -->
        <div class="even">
            <a class="blog-more" href="index.html#"><i class="fa fa-plus"></i></a>
            <a class="latest-blog-img" href="index.html#" style="background-image: url(img/blog/latest-blog/1.jpg);">
                <span class="image-over-style"></span>
            </a>
            <div class="latest-blog-info">
                <h3><a href="index.html#">Little world among the stalks</a></h3>
                <p>Far far away, behind the word…</p>
            </div>
        </div>
        <div class="even">
            <a class="blog-more" href="index.html#"><i class="fa fa-plus"></i></a>
            <a class="latest-blog-img" href="index.html#" style="background-image: url(img/blog/latest-blog/4.jpg);">
                <span class="image-over-style"></span>
            </a>
            <div class="latest-blog-info">
                <h3><a href="index.html#">Thousand unknown plants</a></h3>
                <p>Far far away, behind the word…</p>
            </div>
        </div>
        <div class="latest-blog-desc">
            <a class="blog-more" href="index.html#"><i class="fa fa-plus"></i></a>
            <a class="latest-blog-img" href="index.html#" style="background-image: url(img/blog/latest-blog/2.jpg);">
                <span class="image-over-style"></span>
            </a>
            <div class="latest-blog-info">
                <h3><a href="index.html#">I throw myself down among</a></h3>
                <p>Far far away, behind the word…</p>
            </div>
        </div>
        <!-- bottom blog end -->
    </div>

    <!-- share-area start -->
    <div class="share-area fix">
        <div class="single-share">
            <div class="flip-box-wrap">
                <div class="share-icon">
                    <a class="su-button" href="index-2.html#">
                        <span><i class="fa fa-facebook"></i></span>
                    </a>
                </div>
                <div class="share-icon-text">
                    <a href="index-2.html#"><span>Like On Facebook</span></a>
                </div>
            </div>
        </div>
        <div class="single-share">
            <div class="flip-box-wrap">
                <div class="share-icon">
                    <a class="su-button" href="index-2.html#">
                        <span><i class="fa fa-twitter"></i></span>
                    </a>
                </div>
                <div class="share-icon-text">
                    <a href="index-2.html#"><span> Follow on Twitter</span></a>
                </div>
            </div>
        </div>
        <div class="single-share">
            <div class="flip-box-wrap">
                <div class="share-icon">
                    <a class="su-button" href="index-2.html#">
                        <span><i class="fa fa-google-plus"></i></span>
                    </a>
                </div>
                <div class="share-icon-text">
                    <a href="index-2.html#"><span> Stay in Circle</span></a>
                </div>
            </div>
        </div>
    </div>
    <!-- share-area start -->
@endsection

