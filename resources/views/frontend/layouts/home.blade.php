<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', app_name())</title>

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
        <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
        @yield('meta')

    <!-- Fonts
		============================================ -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700' rel='stylesheet' type='text/css'>

        <!-- Favicon
        ============================================ -->
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

        <!-- CSS  -->

        <!-- Bootstrap CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

        <!-- font-awesome CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">

        <!-- owl.carousel CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}">

        <!-- animate CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">

        <!-- fancybox CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ asset('assets/css/fancybox/jquery.fancybox.css') }}">

        <!-- meanmenu CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ asset('assets/css/meanmenu.min.css') }}">

        <!-- RS slider CSS
        ============================================ -->
        <link rel="stylesheet" type="text/css" href="lib/rs-plugin/css/settings.css" media="screen" />

        <!-- normalize CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">

        <!-- main CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

        <!-- style CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ asset('assets/style.css') }}">

        <!-- responsive CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">

        <!-- modernizr js
        ============================================ -->
        <script src="{{ asset('assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>

        <!-- Styles -->
        @yield('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        @langRTL
            {{ Html::style(getRtlCss(mix('css/frontend.css'))) }}
        @else
            {{ Html::style(mix('css/frontend.css')) }}
        @endif

        @yield('after-styles')

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>

    <body class="home">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!-- header start -->
        <div id="full-content">
            @include('includes.partials.logged-in-as')
            @include('frontend.includes.home-nav')

            <div class="body-content">
                @include('includes.partials.messages')
                @yield('content')
            </div><!-- container -->
        </div><!--#app-->
        <div id="footer-content">
            @include('frontend.includes.footer')
        </div>

        <!-- Scripts -->
        @yield('before-scripts')

        <!-- JS -->

        <!-- jquery js -->
        <script src="{{ asset('assets/js/vendor/jquery-1.11.2.min.js') }}"></script>

        <!-- bootstrap js -->
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

        <!-- owl.carousel.min js -->
        <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>

        <!-- meanmenu js -->
        <script src="{{ asset('assets/js/jquery.meanmenu.js') }}"></script>

        <!-- jquery.countdown js -->
        <script src="{{ asset('assets/js/jquery.countdown.min.js') }}"></script>

        <!-- parallax js -->
        <script src="{{ asset('assets/js/parallax.js') }}"></script>

        <!-- jquery.collapse js -->
        <script src="{{ asset('assets/js/jquery.collapse.js') }}"></script>

        <!-- jquery.easing js -->
        <script src="{{ asset('assets/js/jquery.easing.1.3.min.js') }}"></script>

        <!-- jquery.scrollUp js -->
        <script src="{{ asset('assets/js/jquery.scrollUp.min.js') }}"></script>

        <!-- knob circle js -->
        <script src="{{ asset('assets/js/jquery.knob.js') }}"></script>

        <!-- jquery.appear js -->
        <script src="{{ asset('assets/js/jquery.appear.js') }}"></script>

        <!-- jquery.mixitup js -->
        <script src="{{ asset('assets/js/jquery.mixitup.min.js') }}"></script>

        <!-- fancybox js -->
        <script src="{{ asset('assets/js/fancybox/jquery.fancybox.pack.js') }}"></script>

        <!-- jquery.counterup js -->
        <script src="{{ asset('assets/js/jquery.counterup.min.js') }}"></script>
        <script src="{{ asset('assets/js/waypoints.min.js') }}"></script>

        <!-- rs-plugin js -->
        <script type="text/javascript" src="{{ asset('assets/lib/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/lib/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
        <script src="{{ asset('assets/lib/rs-plugin/rs.home.js') }}"></script>


        <!-- Google Map js -->
        <script src="https://maps.googleapis.com/maps/api/js"></script>
        <script>
            function initialize() {
                var mapOptions = {
                    zoom: 15,
                    scrollwheel: false,
                    center: new google.maps.LatLng(40.663293, -73.956351)
                };

                var map = new google.maps.Map(document.getElementById('googleMap'),
                    mapOptions);


                var marker = new google.maps.Marker({
                    position: map.getCenter(),
                    animation:google.maps.Animation.BOUNCE,
                    icon: "{{ asset('assets/img/map-marker.png') }}",
                    map: map
                });

            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
        <!-- wow js -->
        <script src="{{ asset('assets/js/wow.js') }}"></script>
        <script>
            new WOW().init();
        </script>
        <script>
            var vid = document.getElementById("bg");
            vid.loop = true;
            vid.muted = true
        </script>

        <!-- plugins js -->
        <script src="{{ asset('assets/js/plugins.js') }}"></script>

        <!-- main js -->
        <script src="{{ asset('assets/js/main.js') }}"></script>

        {!! Html::script(mix('js/frontend.js')) !!}
        @yield('after-scripts')

        @include('includes.partials.ga')


    </body>
</html>