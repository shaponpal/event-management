<!-- Event-area start -->
<div class="service-area fix">
    <div class="container">
        <div class="row item-space">
            <!-- single-service start -->
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="single-service wow fadeInDown" data-wow-duration="1.5s" data-wow-delay=".3s">
                    <div class="service-icon">
                        <img src="img/service/1.svg" alt="" />
                    </div>
                    <div class="service-desc">
                        <h3>Keep Daily Update</h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>
                    </div>
                </div>
            </div>
            <!-- single-service end -->
            <!-- single-service start -->
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="single-service wow fadeInDown" data-wow-duration="1.5s" data-wow-delay=".5s">
                    <div class="service-icon">
                        <img src="img/service/2.svg" alt="" />
                    </div>
                    <div class="service-desc">
                        <h3>All Device Friendly</h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>
                    </div>
                </div>
            </div>
            <!-- single-service end -->
            <!-- single-service start -->
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="single-service wow fadeInDown" data-wow-duration="1.5s" data-wow-delay=".7s">
                    <div class="service-icon">
                        <img src="img/service/3.svg" alt="" />
                    </div>
                    <div class="service-desc">
                        <h3>Crafted With Love</h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>
                    </div>
                </div>
            </div>
            <!-- single-service end -->
        </div>
        <div class="row">
            <!-- single-service start -->
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="single-service wow fadeInDown" data-wow-duration="1.5s" data-wow-delay=".9s">
                    <div class="service-icon">
                        <img src="img/service/4.svg" alt="" />
                    </div>
                    <div class="service-desc">
                        <h3>More Powerful Option</h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>
                    </div>
                </div>
            </div>
            <!-- single-service end -->
            <!-- single-service start -->
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="single-service wow fadeInDown" data-wow-duration="1.5s" data-wow-delay="1.1s">
                    <div class="service-icon">
                        <img src="img/service/5.svg" alt="" />
                    </div>
                    <div class="service-desc">
                        <h3>Great Color Look</h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>
                    </div>
                </div>
            </div>
            <!-- single-service end -->
            <!-- single-service start -->
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="single-service wow fadeInDown" data-wow-duration="1.5s" data-wow-delay="1.3s">
                    <div class="service-icon">
                        <img src="img/service/6.svg" alt="" />
                    </div>
                    <div class="service-desc">
                        <h3>Shopping Cart Included</h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>
                    </div>
                </div>
            </div>
            <!-- single-service end -->
        </div>
    </div>
</div>
<!-- Event-area end -->