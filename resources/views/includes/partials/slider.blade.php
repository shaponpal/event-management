<!-- HOME SLIDER -->

<div class="slider-wrap">
    <div class="fullwidthbanner-container" >
        <div class="fullwidthbanner-4">
            <ul>
                <!-- SLIDE  1-->
                <div class="bg-container">
                    <video id="bg" autoplay>
                        <source src="{{ asset('assets/video/video.mp4') }}" type="video/mp4">
                    </video>
                </div>
                <li id="vedio-background">
                    <!-- video-area start -->
                    <div class="video-area-5 fix">

                        <div class="video-text">
                            <h1 class="wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".3s">CLASSIC <strong><span class="typewrite" data-period="2000" data-type='[ " COLORFUL"," EXCELLENT"," BEAUTIFUL" ]'>
							<span class="wrap"></span>
						</span></strong> DRESSES</h1>
                            <p class="wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".6s">Bootstrap is best design ever design from bdthemes ltd, fully hand crafted with huge taste :)</p>
                            <a class="read-more wow fadeInRight" href="index-4.html#" data-wow-duration="1.5s" data-wow-delay=".9s">Purchase Now</a>
                        </div>
                    </div>
                    <!-- video-area end -->
                </li>


            </ul>
        </div>
    </div>
</div>
<!-- END HOME SLIDER -->